<?php

namespace Isobar\Kcp\Controller\Ajax;

class PrepareData extends \Magento\Framework\App\Action\Action
{
    protected $kcpHelper;
    protected $resultJsonFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Isobar\Kcp\Helper\Data $kcpHelper
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->kcpHelper = $kcpHelper;
        parent::__construct($context);
    }

    /**
     * Example index action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $postParams = $this->getRequest()->getParams();
        $paymentPost = $this->getRequest()->getParam('payment');
        $commonData = $this->kcpHelper->initCommonData($paymentPost);
        $orderData = $this->kcpHelper->initOrderData($paymentPost, $postParams);
        $additionalData = $this->kcpHelper->initAdditionalData();
        $mergeData = array_merge($commonData, $orderData);
        $returnData = array_merge($mergeData, $additionalData);
        $result = $this->resultJsonFactory->create();
        return $result->setData($returnData);
    }
}