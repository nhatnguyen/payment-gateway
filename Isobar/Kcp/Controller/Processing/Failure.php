<?php

namespace Isobar\Kcp\Controller\Processing;

class ReturnAction extends \Magento\Framework\App\Action\Action
{
    const LOG_FILE_PATH = '/var/log/Isobar_Kcp_Payment_Error.log';
    protected $kcpHelper;
    protected $kcpApi;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Isobar\Kcp\Helper\Data $kcpHelper,
        \Isobar\Kcp\Model\Api $kcpApi
    ) {
        $this->kcpHelper = $kcpHelper;
        parent::__construct($context);
    }

    /**
     * Example index action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->kcpHelper->log('-- Called :: ' . $this->getRequest()->getActionName() . ' action', self::LOG_FILE_PATH);
        $returnData     = $this->getRequest()->getPost();
        $resCd         = $this->getRequest()->getParam('res_cd');
        $resMsg         = $this->getRequest()->getParam('res_msg');
        $this->messageManager->addError($resMsg);
        $this->kcpHelper->log('Error Code => '.$resCd, self::LOG_FILE_PATH);
        $this->kcpHelper->log('Error Msg => '.$resMsg, self::LOG_FILE_PATH);
        $this->kcpHelper->log('-- END    :: ' . $this->getRequest()->getActionName() . ' action', self::LOG_FILE_PATH);
        $this->_redirect('checkout/cart', array('_secure' => true));
    }
}
