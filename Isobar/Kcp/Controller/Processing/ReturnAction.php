<?php

namespace Isobar\Kcp\Controller\Processing;

use Symfony\Component\Config\Definition\Exception\Exception;
use Magento\Checkout\Model\PaymentInformationManagement;
use Magento\Checkout\Model\GuestPaymentInformationManagement;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender as InvoiceSender;
class ReturnAction extends \Magento\Framework\App\Action\Action
{
    protected $kcpHelper;
    protected $kcpApi;
    protected $customerSession;
    protected $quoteManagement;
    protected $checkoutSession;
    protected $checkoutCart;
    protected $paymentInformationManagement;
    protected $guestPaymentInformationManagement;
    protected $quoteAddress;
    protected $paymentTransaction;
    protected $transactionBuilder;
    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $_transaction;
    protected $invoiceSender;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Isobar\Kcp\Helper\Data $kcpHelper,
        \Isobar\Kcp\Model\Api $kcpApi,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Model\Cart $checkoutCart,
        PaymentInformationManagement $paymentInformationManagement,
        \Magento\Quote\Api\Data\AddressInterface $quoteAddress,
        GuestPaymentInformationManagement $guestPaymentInformationManagement,
        \Magento\Sales\Model\Order\Payment\Transaction $paymentTransaction,
        \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $dbTransaction,
        InvoiceSender $invoiceSender

    ) {
        $this->invoiceSender = $invoiceSender;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $dbTransaction;
        $this->transactionBuilder = $transactionBuilder;
        $this->paymentTransaction = $paymentTransaction;
        $this->quoteAddress = $quoteAddress;
        $this->paymentInformationManagement = $paymentInformationManagement;
        $this->guestPaymentInformationManagement = $guestPaymentInformationManagement;
        $this->customerSession = $customerSession;
        $this->quoteManagement = $quoteManagement;
        $this->checkoutSession = $checkoutSession;
        $this->checkoutCart = $checkoutCart;
        $this->kcpHelper = $kcpHelper;
        /* KCP API 호출 */
        $this->kcpApi = $kcpApi;
        parent::__construct($context);
    }

    /**
     * Example index action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {


        $orderId = $this->_saveOrder();
        //$this->_success($orderId);
        //$this->_success()
        die();
        $this->kcpHelper->log('-- Called :: ' . $this->getRequest()->getActionName() . ' action');
        /* ============================================================================== */
        /* =   01. 지불 요청 정보 설정                                                  = */
        /* = -------------------------------------------------------------------------- = */
        $reqTx = $this->getRequest()->getParam('req_tx');// 요청 종류
        $tranCd = $this->getRequest()->getParam('tran_cd');// 처리 종류
        $traceNo       = "";
        $custIp        = getenv( "REMOTE_ADDR"    ); // 요청 IP
        $orderId = $this->getRequest()->getParam('ordr_idxx');// 쇼핑몰 주문번호
        $goodMny = $this->getRequest()->getParam('good_mny');// 결제 총금액
        $usePayMethod = $this->getRequest()->getParam('use_pay_method');// 결제 방법
        $encData = $this->getRequest()->getParam('enc_data');
        $encInfo = $this->getRequest()->getParam('enc_info');
        $cashYn = $this->getRequest()->getParam('cash_yn');
        $resCd         = "";                         // 응답코드
        $resMsg        = "";                         // 응답메시지

        /* Encrypt Data 설정 && 결제금액 유효성 검증 */
        if ( $reqTx == "pay" )
        {
            $this->kcpApi->setEncData($encData, $encInfo);
            $this->kcpApi->orderTotalVerification($goodMny);
        }

        /* 결제 실행 */
        if ( $tranCd != "" )
        {
            $this->kcpApi->launch($traceNo, $tranCd, $orderId, $custIp);
            $resCd  = $this->kcpApi->getResCode();
        }
        else
        {
            $resCd  = "9562";
            $resMsg = "연동 오류|Payplus Plugin이 설치되지 않았거나 tran_cd값이 설정되지 않았습니다.";
            $this->kcpApi->plugInError($resCd, $resMsg);
            $this->_forward('failure');
        }


        /* 승인 결과 값 추출 및 DB 처리 */
        if ( $reqTx == "pay" )
        {
            if ($resCd == "0000")
            {
                try
                {
                    $_POST['payment']['method'] = \Isobar\Kcp\Model\Ui\ConfigProvider::CODE;
                    $_POST['payment']['kcp_payment_type'] = $usePayMethod;

                    $this->kcpHelper->log('===============================================================');
                    $this->kcpHelper->log('KCP Post Data'.PHP_EOL);
                    $this->kcpHelper->log($this->getRequest()->getPost());
                    $this->kcpHelper->log('===============================================================');
                    $mobile = $this->getRequest()->getParam('param_opt_1') ? "Mobile" : ""; // Moblie 유무

                    $this->_saveOrder();
                    $this->_success($usePayMethod, $orderId, $mobile, $cashYn);
                    $this->_redirect("checkout/onepage/success", array("_secure" => true));

                }catch(Exception $e){
                    $this->kcpApi->autoFail($traceNo, $orderId, $custIp);
                    $_POST['res_cd']      = $this->kcpApi->getResCode();
                    $_POST['res_msg']     = "DB 결과 처리 오류 - 자동 취소";
                    $this->_forward('close');
                }
            }
            else{
                $_POST['res_cd']      = $this->kcpApi->getResCode();
                $_POST['res_msg']     = $this->kcpApi->getResMsg();
                $this->_forward('failure');
            }
        }

        $this->kcpHelper->log('Result Code : ' . PHP_EOL);
        $this->kcpHelper->log($this->kcpApi->getResCode());
        $this->kcpHelper->log('Result Msg : ' . PHP_EOL);
        $this->kcpHelper->log($this->kcpApi->getResMsg());
        $this->kcpHelper->log('-- END :: ' . $this->getRequest()->getActionName() . ' action');
    }

    protected function _saveOrder()
    {
        $quote = $this->checkoutSession->getQuote();


        $payment = $quote->getPayment();

        $payment->setData('kcp_additional_data', 'this is kcp additional data');
        $payment->setData('kcp_payment_type', 'this is kcp payment type');
        $payment->save();

        $quoteBillingAddress = $this->_getBillingAddress($quote);
        if (!$this->customerSession->isLoggedIn()) {
            $cartId = $this->getRequest()->getParam('cartId');
            $email = $this->getRequest()->getParam('guestEmail');
            $orderId = $this->guestPaymentInformationManagement->savePaymentInformationAndPlaceOrder($cartId, $email, $payment, $quoteBillingAddress);
        } else {
            $cartId = $quote->getId();
            $orderId = $this->paymentInformationManagement->savePaymentInformationAndPlaceOrder($cartId, $payment, $quoteBillingAddress);
        }
        return $orderId;
    }

    protected function _getBillingAddress($quote)
    {
        $billingAddess = $quote->getBillingAddress();
        $quoteAddress =  $this->quoteAddress;
        $quoteAddress->setFirstname($billingAddess->getFirstname());
        $quoteAddress->setLastname($billingAddess->getLastname());
        $quoteAddress->setStreet($billingAddess->getStreet());
        $quoteAddress->setCity($billingAddess->getCity());
        $quoteAddress->setTelephone($billingAddess->getTelephone());
        $quoteAddress->setPostcode($billingAddess->getPostcode());
        $quoteAddress->setRegionId($billingAddess->getRegionId());
        $quoteAddress->setCountryId($billingAddess->getCountryId());
        return $quoteAddress;
    }

    protected function _saveOrder1()
    {
        $transactionHardCode = '1AB23456C7890123D';
        if ($this->getRequest()->isAjax()) {
            //return;
        }
        try {
            $quote = $this->checkoutSession->getQuote();
            if (!$this->customerSession->isLoggedIn()) {
                $quote->setCustomerIsGuest(1);
                $quote->setCheckoutMethod('guest');
                $quote->setCustomerId(null);
                $quote->setCustomerEmail($quote->getBillingAddress()->getEmail());
                $quote->setCustomerGroupId(\Magento\Customer\Api\Data\GroupInterface::NOT_LOGGED_IN_ID);
            }
            $quote->setInventoryProcessed(false);
            $quote->save();
            $quote->getPayment()->importData([
                'method' => \Isobar\Kcp\Model\Ui\ConfigProvider::CODE,
                'last_trans_id' => $transactionHardCode,
                'cc_trans_id' => $transactionHardCode,
            ]);
            // Collect Totals & Save Quote
            $quote->collectTotals()->save();
            $quoteId = $quote->getId();
            // Create Order From Quote
            $order = $this->quoteManagement->submit($quote);
            if (null == $order) {
                throw new LocalizedException(
                    __('An error occurred on the server. Please try to place the order again.')
                );
            }
            $payment = $order->getPayment();
            //$payment->setCcLast4($ccLast4);
            $payment->setLastTransId($transactionHardCode);
            $payment->setAdditionalData($transactionHardCode.'additional data');
            $payment->save();

            $this->checkoutSession->setLastSuccessQuoteId($quote->getId());
            $this->checkoutSession->setLastQuoteId($quote->getId());
            $this->checkoutSession->setLastOrderId($order->getId());
            $this->checkoutSession->setLastOrderStatus($order->getStatus());
            $this->checkoutSession->setLastRealOrderId($order->getRealOrderId());
            //$this->checkoutCart->truncate()->save();
            $this->messageManager->addSuccess(__('Order have created successful.'));
            //$this->_redirect('checkout/onepage/success');
            return;
        } catch (\Exception $e) {
            echo $e->getMessage(); die();
            $this->messageManager->addExceptionMessage(
                $e,
                __('We can\'t place the order.')
            );
            $this->_redirect('checkout/cart');
        }
    }

    /**
     * KCP Success :: Magento DB process
     *
     * @params $use_pay_method, $order_id, $mobile, $cash_yn
     */
    protected function _success1($use_pay_method, $order_id, $mobile, $cash_yn)
    {
        $this->kcpHelper->log('-- Called :: ' . __METHOD__ );

        $order           = $this->checkoutSession->getLastRealOrder();
        $kcpHelper       = $this->kcpHelper;
        $transactionType = \Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE;

        $order_state     = $this->kcpHelper->getNewOrderState();
        $kcpPaymentTitle = $this->kcpHelper->getKcpPaymentTypeTitle($use_pay_method);
        $order_msg       = "Payment completed via Kcp $mobile(".$kcpPaymentTitle."). ";

        /* 가상계좌 ORDER 처리 */
        if ( $use_pay_method == "001000000000" )
        {
            $order_msg      = "Payment Pending via Kcp $mobile (".$kcpPaymentTitle.").";
            $order_state    = \Magento\Sales\Model\Order::STATE_NEW;
            $transactionType= \Magento\Sales\Model\Order\Payment\Transaction::TYPE_AUTH;
        }

        $paymentData = $this->kcpApi->getResTotalData($use_pay_method, $order_id, $mobile, $cash_yn);
        $this->kcpHelper->log('===============================================================');
        $this->kcpHelper->log('Payment Data => ');
        $this->kcpHelper->log($paymentData);
        $this->kcpHelper->log('===============================================================');

        $transactionId  = $this->_saveTransaction($use_pay_method, $transactionType, $order, $paymentData);
        if($use_pay_method != '001000000000'){
            $this->_createInvoice($order, $transactionId);
        }

        $order->setState($order_state, true, $order_msg, true);
        $order->save();
        $this->kcpHelper->log('-- END    :: ' . __METHOD__ );
    }

    /**
     * Save Transaction
     *
     * @param $use_pay_method
     * @param $transactionType
     * @param $order
     * @param $paymentData
     * @return bool|string
     */
    protected function _saveTransaction1($use_pay_method, $transactionType, $order, $paymentData)
    {
        $this->kcpHelper->log('-- Called :: ' . __METHOD__ );

        $return = "";

        if($order->getId())
        {
            $setIsClosed = 0;
            $payment = $order->getPayment();
            $tranMsg = "Kcp Transaction Data Save. ($transactionType). ";

            $transactions = $this->paymentTransaction->getCollection()
                ->addFieldToFilter('order_id', array('eq' => $order->getId()))
                ->addFieldToFilter('txn_type', array('eq' => $transactionType))
                ->addFieldToFilter('txn_id', array('eq' => $paymentData['tno']));

            if (count($transactions) > 0) {
                return false;
            }

            try {
                if ($transactionType == \Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE) {
                    $setIsClosed = 1;
                    if($use_pay_method == "001000000000"){
                        $tranMsg .= __("Deposit Amount")." : ".$paymentData['ipgm_mnyx'].". ";
                    }
                }

                $payment->setTransactionId($paymentData['tno']);
                $payment->setData('kcp_payment_type', $use_pay_method);
                $payment->setData('kcp_additional_data', json_encode($paymentData));
                $transaction = $payment->addTransaction($transactionType, null, false, $tranMsg);
                $transaction->setIsClosed($setIsClosed);

                if (is_array($paymentData)) {
                    $transaction->setAdditionalInformation(
                        \Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS,
                        $paymentData
                    );
                }

                $transaction->save();
                $payment->save();

                $return = $payment->getTransactionId();

            } catch (Exception $e) {
                $this->kcpHelper->log($e->getMessage());
            }
        }
        $this->kcpHelper->log('-- END    :: ' . __METHOD__);

        return $return;
    }

    protected function _saveTransaction($transactionType='capture', $order, $paymentData = [])
    {
        try {
            //get payment object from order object
            $payment = $order->getPayment();
            $payment->setLastTransId('1AB23456C7890123D');
            $payment->setTransactionId('1AB23456C7890123D');
            $payment->setData('kcp_payment_type', 'card');
            $payment->setData('kcp_additional_data', json_encode([1,2,3,4,5]));
            $payment->setAdditionalInformation(
                [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
            );
            $formatedPrice = $order->getBaseCurrency()->formatTxt(
                $order->getGrandTotal()
            );

            $message = __('The authorized amount is %1.', $formatedPrice);
            //get the object of builder class
            $trans = $this->transactionBuilder;
            $transaction = $trans->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId('1AB23456C7890123D')
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
                )
                ->setFailSafe(true)
                //build method creates the transaction and returns the object
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);

            $payment->addTransactionCommentsToOrder(
                $transaction,
                $message
            );
            $payment->setParentTransactionId(null);
            $payment->save();
            $order->save();

            return  $transaction->save()->getTransactionId();
        } catch (Exception $e) {
            //log errors here
        }
    }

    protected function _createInvoice($order, $transactionId)
    {
        if($order->canInvoice()) {
            $invoice = $this->_invoiceService->prepareInvoice($order);
            $invoice->register();
            $invoice->setTransactionId('1AB23456C7890123D');
            $invoice->save();
            $transactionSave = $this->_transaction->addObject(
                $invoice
            )->addObject(
                $invoice->getOrder()
            );
            $transactionSave->save();
            $this->invoiceSender->send($invoice);
            //send notification code
            $order->addStatusHistoryComment(
                __('Notified customer about invoice #%1.', $invoice->getId())
            )
                ->setIsCustomerNotified(true)
                ->save();
        }
    }
    protected function _success($order_id)
    {
        $this->kcpHelper->log('-- Called :: ' . __METHOD__ );

        $order           = $this->checkoutSession->getLastRealOrder();
        $kcpHelper       = $this->kcpHelper;
        $transactionType = \Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE;

        $order_state     = $this->kcpHelper->getNewOrderState();

        $transactionId  = $this->_saveTransaction('capture', $order, []);

        $this->_createInvoice($order, $transactionId);

        $order->setState('processing');
        $order->save();
        $this->kcpHelper->log('-- END    :: ' . __METHOD__ );
    }
}
