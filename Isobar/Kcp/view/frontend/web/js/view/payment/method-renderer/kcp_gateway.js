/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'ko',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/action/set-billing-address',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/action/set-payment-information',
    ],
    function ($,
              ko,
              quote,
              Component,
              additionalValidators,
              customerData,
              setBillingAddress,
              customer,
              fullScreenLoader,
              setPaymentInformationAction
        ) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Isobar_Kcp/payment/form',
                paymentTypes: {}
            },
            placeOrderHandler: null,
            validateHandler: null,
            paymentTypes : {},
            prepareDataUrl: null,
            paymentTypeSelected: null,
            initialize: function () {
                this._super();
                this.paymentTypes = window.checkoutConfig.payment.kcp_gateway.kcp_paymentTypes;
                this.prepareDataUrl = window.checkoutConfig.payment.kcp_gateway.prepareDataUrl;
            },
            initObservable: function () {

                this._super()
                    .observe([
                        'paymentTypes'
                    ]);
                return this;
            },
            /**
             * @param {Object} handler
             */
            setPlaceOrderHandler: function (handler) {
                this.placeOrderHandler = handler;
            },

            /**
             * @param {Object} handler
             */
            setValidateHandler: function (handler) {
                this.validateHandler = handler;
            },
            getCode: function() {
                return 'kcp_gateway';
            },

            getData: function() {
                return {
                    'method': this.getCode(),
                    /*'additional_data': {
                        'kcp_paymentTypes': this.paymentTypeSelected
                    }*/
                };
            },

            getPaymentTypes: function() {
                return this.paymentTypes;
            },

            requestKcpPayment: function() {
                var self = this;

                if (additionalValidators.validate()) {
                    if (!self.validateSelectedPaymentTypes()) {
                        alert('Please choose a payment type.');
                    } else {

                        $.when(
                            setBillingAddress()
                        ).done(
                            function () {
                                $.when(
                                    setPaymentInformationAction(this.messageContainer, self.getData())
                                ).done(
                                    function () {
                                        var formData = $('#co-payment-form').serialize();
                                        var data = {};
                                        if (quote.guestEmail) {
                                            formData += '&guestEmail=' + quote.guestEmail;
                                            formData += '&cartId=' + quote.getQuoteId;
                                            var data = {'guestEmail': quote.guestEmail, 'cartId': quote.getQuoteId()}
                                        }
                                        /*$.ajax({url: self.prepareDataUrl,
                                             type: 'POST',
                                             data: formData,
                                             showLoader: true,
                                             success: function (result) {
                                                 var form = $(document.createElement('form'));
                                                 $(form).attr("action", "");
                                                 $(form).attr("method", "POST");
                                                 $(form).attr("name", "order_info");
                                                 for(var k in result) {
                                                    $(form).append('<input type="hidden" name="'+k+'" value="'+result[k]+'"/>');
                                                 }
                                                 KCP_Pay_Execute(form[0]);
                                                 fullScreenLoader.stopLoader();
                                             }
                                         });*/
                                        $.ajax({url: '/isobar_kcp/processing/return',
                                            type: 'POST',
                                            data: data,
                                            showLoader: true,
                                            success: function (result) {
                                                var sections = ['cart'];
                                                customerData.invalidate(sections);
                                                customerData.reload(sections, true);
                                                window.top.location.href = "http://local.dev.sample/checkout/onepage/success";
                                            }
                                        });
                                    }
                                );
                            }
                        );
                    }
                }
            },
            validateSelectedPaymentTypes: function () {
                var self = this;
                var flag = false;
                var paymentTypesSelected = $('[name="payment[type]"]');
                $.each(paymentTypesSelected, function(index, paymentType) {
                    if ($(paymentType).is(':checked')) {
                        flag = true;
                        self.paymentTypeSelected = $(paymentType).val();
                        return false;
                    }
                });
                return flag;
            },

        });
    }
);