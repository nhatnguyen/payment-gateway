<?php
namespace Isobar\Kcp\Helper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Isobar\Kcp\Model\Adminhtml\Source\PaymentTypes;
use \Magento\Checkout\Model\Session as CheckoutSession;
use \Magento\Directory\Helper\Data as HelperDirectory;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const LOG_FILE_PATH = '/var/log/Isobar_Kcp_Payment.log';
    protected $paymentTypesObj;
    protected $scopeConfig;
    protected $helperConfig;
    protected $checkoutSession;
    protected $storeManager;
    protected $helperDirectory;
    protected $urlBuilder;
    protected $request;
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\UrlInterface $urlBuilder,
        HelperDirectory $helperDirectory,
        PaymentTypes $paymentTypes,
        \Isobar\Kcp\Helper\Config $helperConfig,
        CheckoutSession $checkoutSession
    ) {
        $this->request = $request;
        $this->urlBuilder = $urlBuilder;
        $this->storeManager = $storeManager;
        $this->helperDirectory = $helperDirectory;
        $this->scopeConfig = $scopeConfig;
        $this->paymentTypesObj = $paymentTypes;
        $this->helperConfig = $helperConfig;
        $this->checkoutSession = $checkoutSession;
    }

    public function log($data, $logFilePath = null)
    {
        $filePath = $logFilePath ? $logFilePath : self::LOG_FILE_PATH;
        $writer = new \Zend\Log\Writer\Stream(BP . $filePath);
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($data, true));
    }

    public function getSupportPaymentTypes()
    {
        $paymentTypeConfig = $this->helperConfig->getKcpStoreConfig('payment_types');
        $paymentTypeValues = explode(',', $paymentTypeConfig);
        $paymentTypes = $this->paymentTypesObj->toArray();
        $paymentTypesData = [];
        $i = 0;
        foreach ($paymentTypes as $value => $label) {
            if (in_array($value, $paymentTypeValues)) {
                $checked = $i == 0 ? true : false;
                $paymentTypesData[] = ['label' => $label, 'type_value' => $value, 'is_checked' => $checked];
            }
            $i++;
        }
        return $paymentTypesData;
    }

    public function initCommonData($kcpPayment)
    {
        $initCommonData = array(
            'site_cd'       => $this->helperConfig->getMertId(),
            'site_name'     => $this->helperConfig->getSiteName(),
            'site_logo'     => $this->helperConfig->getSiteLogo(),
            'module_type'   => $this->helperConfig->getKcpStoreConfig('module_type'),
            'skin_indx'          => $this->helperConfig->getKcpStoreConfig('skin'),
            'eng_flag'      => $this->helperConfig->getKcpEngFlag()
        );
        if (isset($kcpPayment['type']) && '001000000000' == $kcpPayment['type']) {
            $initCommonData['vcnt_expire_term'] = $this->helperConfig->getVBankExpireTerm();
        }
        return $initCommonData;
    }

    /** KCP Order Data Initialize */
    public function initOrderData($kcpPayment, $postParams = [])
    {
        $quote          = $this->checkoutSession->getQuote();
        $payment        = $quote->getPayment();
        $shipping       = $quote->getShippingAddress();
        $userId        = is_null($quote->getCustomerId()) ? "Guest" : $quote->getCustomerId();

        if (!isset($kcpPayment['type'])) {
            throw new \Magento\Framework\Exception\LocalizedException(__('No KCP payment method applied'));
        }
        $initOrderData = array(
            'pay_method'    => $kcpPayment['type'],
            'ordr_idxx'     => $this->helperConfig->getOrderPreFix() . $quote->getId(),
            'good_name'     => $this->getGoodName($quote),
            'good_mny'      => (int)$this->_checkCurrency($quote->getGrandTotal()),
            'buyr_name'     => $quote->getShippingAddress()->getFirstname()." ".$quote->getShippingAddress()->getLastname(),
            'buyr_mail'     => isset($postParams['guestEmail']) ? $postParams['guestEmail'] : $quote->getCustomerEmail(),
            'buyr_tel1'     => $shipping->getTelephone(),
            'buyr_tel2'     => $shipping->getTelephone(),
            'shop_user_id'  => $userId,
        );

        # Payco
        if(isset($kcpPayment['kcp_payment_payco']))
        {
            $initOrderData['payco_direct'] = 'Y';
        }

        return $initOrderData;
    }

    public function initAdditionalData()
    {
        $additionalData = array(
            'req_tx' => 'pay',
            'quotaopt' => 12,
            'currency' => $this->helperConfig->getKcpCurrency(),
            'tno' => '',
            'res_cd' => '',
            'res_msg' => '',
            'trace_no' => '',
            'enc_info' => '',
            'enc_data' => '',
            'ret_pay_method' => '',
            'tran_cd' => '',
            'bank_issu' => '',
            'use_pay_method' => '',
            'cash_yn' => '',
            'cash_tr_code' => '',
            'cash_id_info' => '',
            'good_expr' => '0',
            'pt_memcorp_cd' => '',
            'return_url'    => $this->urlBuilder->getUrl('isobar_kcp/processing/returnAction')
        );
        return $additionalData;
    }

    protected function getGoodName($quote)
    {
        $items = $quote->getAllItems();
        $count = 0;
        $goodsName = '';

        foreach ($items as $item)
        {
            if($count == 0) {
                $goodsName = $item->getName();
            }
            if($item->getProductType() == 'simple')
                $count++;
        }

        $goodsName = ($count > 1) ? $goodsName.' 외 '.($count-1). '건' : $goodsName;

        return $goodsName;
    }

    /**
     * Web Currency KRW or USD
     *
     * @param $grandTotal
     * @return $amount
     */
    protected function _checkCurrency($grandTotal)
    {
        $currentCurrencyCode = $this->storeManager->getStore()->getCurrentCurrencyCode();
        $amount              = $grandTotal;
        $kcpCurrency         = $this->helperConfig->getKcpCurrency();
        $kcpCurrencyCheck    = $this->_currencyListCheck($kcpCurrency);

        if(!$kcpCurrencyCheck){
            throw new \Magento\Framework\Exception\LocalizedException(__('Currency rate conversion failed, please contact administrator to setup currency rate first.'));
        }

        if($currentCurrencyCode != "KRW" && $kcpCurrency == "WON") {
            $amount = $this->helperDirectory->currencyConvert($grandTotal, $currentCurrencyCode, "KRW");
        }elseif($currentCurrencyCode != "USD" && $kcpCurrency == "USD"){
            $amount = $this->helperDirectory->currencyConvert($grandTotal, $currentCurrencyCode, "USD");
            $amount *= 100;
        }elseif($currentCurrencyCode == "USD" && $kcpCurrency == "USD"){
            $amount *= 100;
        }

        return $amount;
    }

    /**
     * Check Currency List
     *
     * @param $kcpCurrency
     * @return string($check)
     */
    protected function _currencyListCheck($kcpCurrency)
    {
        $availableCurrency = $this->storeManager->getStore()->getAvailableCurrencyCodes();
        $currency          = ($kcpCurrency == 'WON') ? 'KRW' : $kcpCurrency;
        $check             = in_array($currency, $availableCurrency);
        return $check;
    }

    public function getNewOrderState()
    {
        $orderState = $this->helperConfig->getKcpStoreConfig('order_status');
        if ($orderState == "pending"){
            return \Magento\Sales\Model\Order::STATE_NEW;
        } else {
            return \Magento\Sales\Model\Order::STATE_PROCESSING;
        }
    }

    public function getKcpPaymentTypeTitle($type)
    {
        $paymentTypes     = $this->getPaymentTypes();
        $paymentMobiTypes =  $this->getMobiPaymentTypes();
        $title            = "";

        if($type == ""){
            return $title;
        }

        if(isset($paymentTypes[$type])){
            $title = $paymentTypes[$type];
        }elseif(isset($paymentMobiTypes[$type])){
            $title = $paymentMobiTypes[$type];
        }
        return $title;
    }

    public function getPaymentTypes()
    {
        return array(
            '100000000000' => __('Card'),
            '010000000000' => __('Bank'),
            '001000000000' => __('VBank'),
            '000100000000' => __('Point'),
            '000010000000' => __('Mobile Payment'),
            '000000001000' => __('Gift'),
            '000000000010' => __('ARS'),
            '111000000000' => __('Card/Bank/VBank')
        );
    }

    public function getMobiPaymentTypes()
    {
        return array(
            'card' => __('Card'),
            'acnt' => __('Bank'),
            'vcnt' => __('VBank'),
            'tpnt' => __('Point'),
            'mobx' => __('Mobile Payment'),
            'scbl' => __('Book Coupon'),
            'sccl' => __('Culture Coupon'),
            'schm' => __('Happy Money')
        );
    }
}
