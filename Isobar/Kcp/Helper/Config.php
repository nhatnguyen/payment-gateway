<?php
namespace Isobar\Kcp\Helper;
use Magento\Framework\App\Config\ScopeConfigInterface;
class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $scopeConfig;
    protected $storeManager;
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;

    }

    public function getKcpStoreConfig($key, $encrypt = false)
    {
        $value = $this->scopeConfig->getValue('payment/kcp_gateway/' . $key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($encrypt) {
            return $this->_encryptor->decrypt($value);
        }
        return $value;
    }

    public function getMode()
    {
        $testMode = $this->getKcpStoreConfig('testmode');
        $mode = ($testMode == 1) ? 'test' : 'real';

        return $mode;
    }

    public function getMertId()
    {
        $mode       = $this->getMode();
        $mertId = $this->getKcpStoreConfig('merchant_id');
        if($mode == 'test'){
            $mertId = $this->getKcpStoreConfig('test_id');
        }
        return $mertId;
    }

    public function getSiteName()
    {
        $siteName = $this->getKcpStoreConfig('site_name');
        return $siteName ? $siteName : $this->storeManager->getStore()->getName();
    }

    public function getSiteLogo()
    {
        $siteLogo = $this->getKcpStoreConfig('site_logo');
        //$siteLogoUrl = $siteLogo != '' ? Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'kcp_logo/'.$siteLogo : "";
        $siteLogoUrl = '';
        return $siteLogoUrl;
    }

    public function getVBankExpireTerm()
    {
        $expireDate = $this->getKcpStoreConfig('expire_date') ? $this->getKcpStoreConfig('expire_date') : '5';
        return $expireDate;
    }

    public function getKcpEngFlag()
    {
        $engFlagData = array( "0"   => "N", "1"   => "Y" );

        $engFlag = $this->getKcpStoreConfig('eng_flag');

        return $engFlagData[$engFlag];
    }

    public function getOrderPreFix()
    {
        $prefix = $this->getKcpStoreConfig("order_prefix");

        return $prefix;
    }

    public function getKcpCurrency()
    {
        $currencyData = array(
            '1'   => 'WON',
            '2'   => 'USD'
        );

        $currency = $this->getKcpStoreConfig('currency');

        return $currencyData[$currency];
    }

    public function getModeJs()
    {
        $testmode = $this->getKcpStoreConfig('testmode');
        $payplus = $this->getKcpStoreConfig('real_js');
        if($testmode > 0) {
            $payplus = $this->getKcpStoreConfig('test_js');
        }
        return $payplus;
    }
}
