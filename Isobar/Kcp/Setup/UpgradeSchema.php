<?php

namespace Isobar\Kcp\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable('quote_payment'),
            'kcp_additional_data',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Kcp Additional Data',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_payment'),
            'kcp_additional_data',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Kcp Additional Data',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote_payment'),
            'kcp_payment_type',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Kcp Payment Type',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_payment'),
            'kcp_payment_type',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Kcp Payment Type',
            ]
        );

        $installer->endSetup();
    }
}