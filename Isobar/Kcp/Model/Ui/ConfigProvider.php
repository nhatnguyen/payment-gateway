<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Isobar\Kcp\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Isobar\Kcp\Gateway\Http\Client\ClientMock;

/**
 * Class ConfigProvider
 */
final class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'kcp_gateway';

    protected $kcpHelper;
    protected $storeManager;
    public function __construct(
        \Isobar\Kcp\Helper\Data $kcpHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->kcpHelper = $kcpHelper;
        $this->storeManager = $storeManager;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                self::CODE => [
                    'kcp_paymentTypes' => $this->kcpHelper->getSupportPaymentTypes(),
                    'prepareDataUrl' => $this->storeManager->getStore()->getUrl('isobar_kcp/ajax/preparedata')
                ]
            ]
        ];
    }
}
