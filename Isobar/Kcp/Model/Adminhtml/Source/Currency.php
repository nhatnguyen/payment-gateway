<?php
namespace Isobar\Kcp\Model\Adminhtml\Source;

class Currency implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => '1', 'label' => __('WON')),
            array('value' => '2', 'label' => __('USD')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            '1' => __('WON'),
            '2' => __('USD'),
        );
    }
}
