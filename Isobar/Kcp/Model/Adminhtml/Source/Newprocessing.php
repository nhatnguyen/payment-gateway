<?php
namespace Isobar\Kcp\Model\Adminhtml\Source;

class Newprocessing implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => '',
                'label' => __('-- Please Select --')
            ],
            [
                'value' => 'pending',
                'label' => __('Pending')
            ],
            [
                'value' => 'processed_ogone',
                'label' => __('Processed Ogone Payment')
            ],
            [
                'value' => 'processing',
                'label' => __('Processing')
            ],
        ];
    }
}
