<?php
namespace Isobar\Kcp\Model\Adminhtml\Source;

class Skins implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => '1', 'label' => __('DARK CYAN')),
            array('value' => '2', 'label' => __('PURPLE')),
            array('value' => '3', 'label' => __('BROWN')),
            array('value' => '4', 'label' => __('VIOLET RED')),
            array('value' => '5', 'label' => __('DARK SLATE BLUE')),
            array('value' => '6', 'label' => __('STEEL BLUE')),
            array('value' => '7', 'label' => __('DARK GOLDENROD')),
            array('value' => '8', 'label' => __('ORANGE')),
            array('value' => '9', 'label' => __('DARK GOLDENROD')),
            array('value' => '10', 'label' => __('DARK RED')),
            array('value' => '11', 'label' => __('DARK GRAY'))

        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            '1' => __('DARK CYAN'),
            '2' => __('PURPLE'),
            '3' => __('BROWN'),
            '4' => __('VIOLET RED'),
            '5' => __('DARK SLATE BLUE'),
            '6' => __('STEEL BLUE'),
            '7' => __('DARK GOLDENROD'),
            '8' => __('ORANGE'),
            '9' => __('DARK GOLDENROD'),
            '10' => __('DARK RED'),
            '11' => __('DARK GRAY')
        );
    }
}
