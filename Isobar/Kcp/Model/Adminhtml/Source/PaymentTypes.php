<?php
namespace Isobar\Kcp\Model\Adminhtml\Source;

class PaymentTypes implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('label' => __('Card'), 'value' => '100000000000'),
            array('label' => __('Bank'), 'value' => '010000000000'),
            array('label' => __('VBank'), 'value' => '001000000000'),
            array('label' => __('Point'), 'value' => '000100000000'),
            array('label' => __('Mobile'), 'value' => '000010000000'),
            array('label' => __('Gift'), 'value' => '000000001000'),
            array('label' => __('ARS'), 'value' => '000000000010'),
            array('label' => __('Card/Bank/VBank'), 'value' => '111000000000'),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            '100000000000' => __('Card'),
            '010000000000' => __('Bank'),
            '001000000000' => __('VBank'),
            '000100000000' => __('Point'),
            '000010000000' => __('Mobile'),
            '000000001000' => __('Gift'),
            '000000000010' => __('ARS'),
            '111000000000' => __('Card/Bank/VBank')
        );
    }
}
