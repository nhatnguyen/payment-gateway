<?php
namespace Isobar\Kcp\Model\Adminhtml\Source;

class MobiPaymentTypes implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('label' => __('Card'), 'value' => 'card'),
            array('label' => __('Bank'), 'value' => 'acnt'),
            array('label' => __('VBank'), 'value' => 'vcnt'),
            array('label' => __('Point'), 'value' => 'tpnt'),
            array('label' => __('Mobile Payment'), 'value' => 'mobx'),
            array('label' => __('Book Coupon'), 'value' => 'scbl'),
            array('label' => __('Culture Coupon'), 'value' => 'sccl'),
            array('label' => __('Happy Money'), 'value' => 'schm')
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'card' => __('Card'),
            'acnt' => __('Bank'),
            'vcnt' => __('VBank'),
            'tpnt' => __('Point'),
            'mobx' => __('Mobile Payment'),
            'scbl' => __('Book Coupon'),
            'sccl' => __('Culture Coupon'),
            'schm' => __('Happy Money')
        );
    }
}
