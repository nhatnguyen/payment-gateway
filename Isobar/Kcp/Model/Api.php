<?php
/**
 * @category   Seoulwebdesign Extension
 * @package    Seoulwebdesign_Inipay
 * @copyright  Copyright (c) 2014 Seoulwebdesign (http://www.seoulwebdesign.com/).
 */

/*if(!defined("LIB_HOME"))
{
    define("LIB_HOME", Mage::getBaseDir('app') . DS . 'code' . DS . 'local' . DS . 'Eguana' . DS . 'Kcp' . DS . 'lib');
}

require_once LIB_HOME . DS . 'pp_cli_hub_lib.php';*/
namespace Isobar\Kcp\Model;
class Api
{

    protected $c_PayPlus;

    protected $g_conf_home_dir;
    protected $g_conf_site_cd;
    protected $g_conf_site_key;
    protected $g_conf_gw_url;
    protected $g_conf_gw_port;
    protected $g_conf_log_level;
    protected $g_conf_log_path;
    protected $_config_helper;

    protected $t_no;
    protected $m_res_cd;
    protected $m_res_msg;

    // singleton class instance
    private static $_classInstance;

    /*public static function getInstance()
    {
        if (null === self::$_classInstance) {
            self::$_classInstance = new self();
        }

        return self::$_classInstance;
    }*/

    public function __construct(\Isobar\Kcp\Lib\PpCliHubLib $ppCliHubLib)
    {
        //$this->init();

        /* kcp 인스턴스 생성 */
        $this->c_PayPlus = $ppCliHubLib;

        //$this->c_PayPlus->mf_clear();
    }

    protected function init()
    {
        $configData = Mage::helper("kcp/config")->getKcpInitData();

        $this->g_conf_home_dir    = $configData['home_dir'];
        $this->g_conf_site_cd     = $configData['mert_id'];
        $this->g_conf_site_key    = $configData['mert_key'];
        $this->g_conf_gw_url      = $configData['site_url'];
        $this->g_conf_gw_port     = $configData['port'];
        $this->g_conf_log_level   = 3;
        $this->g_conf_log_path    = $configData['log_path'];
    }

    /* 결제 실행 */
    public function launch($trace_no, $tran_cd, $order_id, $cust_ip)
    {
        $this->c_PayPlus->mf_do_tx( $trace_no, $this->g_conf_home_dir, $this->g_conf_site_cd, $this->g_conf_site_key, $tran_cd, "",
            $this->g_conf_gw_url, $this->g_conf_gw_port, "payplus_cli_slib", $order_id,
            $cust_ip, $this->g_conf_log_level, 0, 0, $this->g_conf_log_path );

        $this->setResCode($this->c_PayPlus->m_res_cd);
        $this->setResMsg($this->iConvUTF8($this->c_PayPlus->m_res_msg));
        $this->t_no = $this->c_PayPlus->mf_get_res_data("tno");
    }


    public function autoFail($trace_no,  $order_id, $cust_ip)
    {
        $tran_cd = "00200000";

        $this->c_PayPlus->mf_clear();

        $this->c_PayPlus->mf_set_modx_data("tno", $this->t_no);     // KCP 원거래 거래번호
        $this->c_PayPlus->mf_set_modx_data("mod_type", "STSC");     // 원거래 변경 요청 종류
        $this->c_PayPlus->mf_set_modx_data("mod_ip", $cust_ip);     // 변경 요청자 IP
        $this->c_PayPlus->mf_set_modx_data("mod_desc", "결과 처리 오류 - 자동 취소");     // 변경 사유
        
        $this->c_PayPlus->mf_do_tx( $trace_no, $this->g_conf_home_dir, $this->g_conf_site_cd, $this->g_conf_site_key, $tran_cd, "",
            $this->g_conf_gw_url, $this->g_conf_gw_port, "payplus_cli_slib", $order_id,
            $cust_ip, $this->g_conf_log_level, 0, 0, $this->g_conf_log_path );

        $this->setResCode($this->c_PayPlus->m_res_cd);
        $this->setResMsg($this->iConvUTF8($this->c_PayPlus->m_res_msg));
    }

    public function kcpCancel($tno, $order_id, $cust_ip, $mod_desc)
    {
        $tran_cd  = "00200000";
        $trace_no = "";

        $this->c_PayPlus->mf_clear();

        $this->c_PayPlus->mf_set_modx_data("tno", $tno);              // KCP 원거래 거래번호
        $this->c_PayPlus->mf_set_modx_data("mod_type", "STSC");       // 원거래 변경 요청 종류
        $this->c_PayPlus->mf_set_modx_data("mod_ip", $cust_ip);       // 변경 요청자 IP
        $this->c_PayPlus->mf_set_modx_data("mod_desc", $mod_desc);    // 변경 사유

        $this->c_PayPlus->mf_do_tx( $trace_no, $this->g_conf_home_dir, $this->g_conf_site_cd, $this->g_conf_site_key, $tran_cd, "",
            $this->g_conf_gw_url, $this->g_conf_gw_port, "payplus_cli_slib", $order_id,
            $cust_ip, $this->g_conf_log_level, 0, 0, $this->g_conf_log_path );

        $this->setResCode($this->c_PayPlus->m_res_cd);
        $this->setResMsg($this->iConvUTF8($this->c_PayPlus->m_res_msg));

        $res     = array(
            "res_cd"    => $this->getResCode(),
            "res_msg"   => $this->getResMsg()
        );

        return $res;
    }

    /* 9562 Error 처리 */
    public function plugInError($res_cd, $res_msg){
        $this->c_PayPlus->m_res_cd  = $res_cd;
        $this->c_PayPlus->m_res_msg = $res_msg;
        $this->setResCode($res_cd);
        $this->setResMsg($res_msg);
    }

    /* 결제금액 유효성 검증 */
    public function orderTotalVerification($good_mny){
        $this->c_PayPlus->mf_set_ordr_data( "ordr_mony",  $good_mny);
    }

    /* Encrypt Data 설정 */
    public function setEncData($enc_data, $enc_info){
        $this->c_PayPlus->mf_set_encx_data($enc_data, $enc_info);
    }

    public function getResCode(){
        return $this->m_res_cd;
    }

    public function getResMsg(){
        return $this->m_res_msg;
    }

    public function setResCode($res_cd){
        $this->m_res_cd = $res_cd;
    }

    public function setResMsg($res_msg){
        $this->m_res_msg = $res_msg;
    }

    public function iConvUTF8($value){
        return iconv('euc-kr', 'utf-8', $value);
    }

    public function getResData($key){
        $value = isset($this->c_PayPlus->m_res_data[$key]) ? $this->c_PayPlus->mf_get_res_data($key) : "";
        return $value;
    }

    /* Response KCP Data return Array */
    public function getResTotalData($use_pay_method, $order_id, $mobile, $cash_yn)
    {
        $commonData     = $this->commonResData($order_id, $mobile, $cash_yn);
        $payMethodData  = "";

        # 신용카드
        if ($use_pay_method == "100000000000") {
            $pnt_issue = $commonData['pnt_issue'];
            $payMethodData = $this->cardResData($pnt_issue);
        }
        # 계좌이체
        elseif ($use_pay_method == "010000000000") {
            $payMethodData = $this->bankResData();
        }
        # 가상계좌
        elseif ($use_pay_method == "001000000000") {
            $payMethodData = $this->vBankResData();
        }
        # 포인트
        elseif ($use_pay_method == "000100000000") {
            $payMethodData = $this->pointResData();
        }
        # 휴대폰
        elseif ($use_pay_method == "000010000000") {
            $payMethodData = $this->mobileResData();
        }
        # 상품권
        elseif ($use_pay_method == "000000001000") {
            $payMethodData = $this->giftResData();
        }

        $resTotalData = array_merge($commonData, $payMethodData);
        return $resTotalData;
    }

    protected function commonResData($order_id, $mobile, $cash_yn)
    {
        $commonData = array(
            "tno"       => $this->c_PayPlus->mf_get_res_data("tno"),            // KCP 거래 고유 번호
            "amount"    => $this->c_PayPlus->mf_get_res_data("amount"),         // KCP 실제 거래 금액
            "coupon_mny"=> $this->getResData('coupon_mny'),                     // 쿠폰금액
            "pnt_issue" => $this->getResData('pnt_issue'),                      // 결제 포인트사 코드
            "order_id"  => $order_id,                                           // KCP 주문번호
            "mobile"    => $mobile                                              // Mobile 유무
        );

        if($cash_yn != ""){
            $commonData['cash_yn']      = $cash_yn;                             // 현금 영수증 유무
            $commonData['cash_no']      = $this->getResData("cash_no");         // 현금 영수증 번호
            $commonData['cash_authno']  = $this->getResData("cash_authno");     // 현금 영수증 승인 번호
        }

        return $commonData;
    }

    protected function cardResData($pnt_issue)
    {
        $payMethodData = array(
            "card_cd" => $this->c_PayPlus->mf_get_res_data("card_cd"),                      // 카드사 코드
            "card_name" => $this->iConvUtf8($this->c_PayPlus->mf_get_res_data("card_name")),  // 카드 종류
            "app_time" => $this->c_PayPlus->mf_get_res_data("app_time"),                     // 승인 시간
            "app_no" => $this->c_PayPlus->mf_get_res_data("app_no"),                       // 승인 번호
            "noinf" => $this->c_PayPlus->mf_get_res_data("noinf"),                        // 무이자 여부 ( 'Y' : 무이자 )
            "quota" => $this->c_PayPlus->mf_get_res_data("quota"),                       // 할부 개월 수
            "partcanc_yn" => $this->c_PayPlus->mf_get_res_data("partcanc_yn"),                  // 부분취소 가능유무
            "card_bin_type_01" => $this->c_PayPlus->mf_get_res_data("card_bin_type_01"),             // 카드구분1
            "card_bin_type_02" => $this->c_PayPlus->mf_get_res_data("card_bin_type_02"),             // 카드구분2
            "card_mny" => $this->c_PayPlus->mf_get_res_data("card_mny")                      // 카드결제금액
        );

        /* = --------------------------------------------------------------------- = */
        /* =   05-1.1. 복합결제(포인트+신용카드) 승인 결과 처리                    = */
        /* = --------------------------------------------------------------------- = */
        if ($pnt_issue == "SCSK" || $pnt_issue == "SCWB") {
            $payMethodData["pnt_amount"] = $this->c_PayPlus->mf_get_res_data("pnt_amount");       // 적립금액 or 사용금액
            $payMethodData["pnt_app_time"] = $this->c_PayPlus->mf_get_res_data("pnt_app_time");     // 승인시간
            $payMethodData["pnt_app_no"] = $this->c_PayPlus->mf_get_res_data("pnt_app_no");       // 승인번호
            $payMethodData["add_pnt"] = $this->c_PayPlus->mf_get_res_data("add_pnt");          // 발생 포인트
            $payMethodData["use_pnt"] = $this->c_PayPlus->mf_get_res_data("use_pnt");          // 사용가능 포인트
            $payMethodData["rsv_pnt"] = $this->c_PayPlus->mf_get_res_data("rsv_pnt");          // 총 누적 포인트
            //$total_amount = $payDefaultData['amount'] + $pnt_amount;                          // 복합결제시 총 거래금액
        }

        return $payMethodData;
    }

    protected function bankResData()
    {
        $payMethodData  = array(
            "app_time"  => $this->c_PayPlus->mf_get_res_data("app_time"),                     // 승인 시간
            "bank_name" => $this->iConvUtf8($this->c_PayPlus->mf_get_res_data("bank_name")),  // 은행명
            "bank_code" => $this->c_PayPlus->mf_get_res_data("bank_code"),                    // 은행코드
            "bk_mny"    => $this->c_PayPlus->mf_get_res_data("bk_mny")                        // 계좌이체결제금액
        );

        return $payMethodData;
    }

    protected function vBankResData()
    {
        $payMethodData  = array(
            "bankname"  => $this->iConvUtf8($this->c_PayPlus->mf_get_res_data("bankname")),   // 입금할 은행 이름
            "depositor" => $this->iConvUtf8($this->c_PayPlus->mf_get_res_data("depositor")),  // 입금할 계좌 예금주
            "account"   => $this->c_PayPlus->mf_get_res_data("account"),                      // 입금할 계좌 번호
            "va_date"   => $this->c_PayPlus->mf_get_res_data("va_date")                       // 가상계좌 입금마감시간
        );

        return $payMethodData;
    }

    protected function pointResData()
    {
        $payMethodData  = array(
            "pnt_amount"   => $this->c_PayPlus->mf_get_res_data("pnt_amount"),                // 적립금액 or 사용금액
            "pnt_app_time" => $this->c_PayPlus->mf_get_res_data("pnt_app_time"),              // 승인시간
            "pnt_app_no"   => $this->c_PayPlus->mf_get_res_data("pnt_app_no"),                // 승인번호
            "add_pnt"      => $this->c_PayPlus->mf_get_res_data("add_pnt"),                   // 발생 포인트
            "use_pnt"      => $this->c_PayPlus->mf_get_res_data("use_pnt"),                   // 사용가능 포인트
            "rsv_pnt"      => $this->c_PayPlus->mf_get_res_data("rsv_pnt")                    // 적립 포인트
        );

        return $payMethodData;
    }

    protected function mobileResData()
    {
        $payMethodData  = array(
            "app_time"  => $this->c_PayPlus->mf_get_res_data("hp_app_time"),                  // 승인 시간
            "commid"    => $this->c_PayPlus->mf_get_res_data("commid"),                       // 통신사 코드
            "mobile_no" => $this->c_PayPlus->mf_get_res_data("mobile_no")                     // 휴대폰 번호
        );

        return $payMethodData;
    }

    protected function giftResData()
    {
        $payMethodData  = array(
            "app_time"    => $this->c_PayPlus->mf_get_res_data("tk_app_time"),                // 승인 시간
            "tk_van_code" => $this->c_PayPlus->mf_get_res_data("tk_van_code"),                // 발급사 코드
            "tk_app_no"   => $this->c_PayPlus->mf_get_res_data("tk_app_no")                   // 승인 번호
        );

        return $payMethodData;
    }
}
