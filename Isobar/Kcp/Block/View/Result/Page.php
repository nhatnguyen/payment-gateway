<?php

namespace Isobar\Kcp\Block\View\Result;


class Page extends \Magento\Framework\View\Result\Page
{
    public function allowKcpJsLoading()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $kcpConfig = $objectManager->create('Isobar\Kcp\Helper\Config');
        $fullActionName = $this->request->getModuleName() . '_' . $this->request->getControllerName() . '_' . $this->request->getActionName();
        if ('checkout_index_index' === $fullActionName) {
            if (0 < $kcpConfig->getKcpStoreConfig('active')) {
                return $kcpConfig->getModeJs();
            }
        }
        return '';
    }
}
